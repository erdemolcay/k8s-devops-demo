- name: Enable necessary kernel modules
  ansible.builtin.lineinfile:
    path: /etc/modules-load.d/containerd.conf
    line: "{{ item }}"
    create: true
    mode: "0644"
    owner: root
    group: root
  loop:
    - overlay
    - br_netfilter

- name: Load necessary kernel modules
  community.general.modprobe:
    name: "{{ item }}"
    state: present
  loop:
    - overlay
    - br_netfilter

- name: Set necessary kernel parameters
  ansible.posix.sysctl:
    name: "{{ item.name }}"
    value: "{{ item.value }}"
    state: present
    sysctl_set: true
  loop:
    - name: net.bridge.bridge-nf-call-ip6tables
      value: "1"
    - name: net.bridge.bridge-nf-call-iptables
      value: "1"
    - name: net.ipv4.ip_forward
      value: "1"

- name: Install packages that allow apt to be used over HTTPS
  ansible.builtin.apt:
    name: "{{ packages }}"
    state: present
    update_cache: true
  vars:
    packages:
      - gnupg2
      - software-properties-common
      - apt-transport-https
      - ca-certificates
      - curl

- name: Add apt signing key for Docker repository
  ansible.builtin.apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    state: present

- name: Add Docker apt repository
  ansible.builtin.apt_repository:
    repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} stable
    state: present

- name: Install containerd
  ansible.builtin.apt:
    name: containerd.io
    state: present
    update_cache: true

- name: Get default containerd configuration
  ansible.builtin.command: containerd config default
  register: output

- name: Save containerd configuration
  ansible.builtin.copy:
    content: "{{ output.stdout }}"
    dest: /etc/containerd/config.toml
    mode: "0644"
    owner: root
    group: root

- name: Enable SystemdCgroup in containerd configuration
  ansible.builtin.replace:
    path: /etc/containerd/config.toml
    regexp: SystemdCgroup = false
    replace: SystemdCgroup = true

- name: Enable and restart containerd service
  ansible.builtin.service:
    name: containerd
    state: restarted
    enabled: true

- name: Add apt signing key for Kubernetes repository
  ansible.builtin.apt_key:
    url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
    state: present

- name: Add Kubernetes apt repository
  ansible.builtin.apt_repository:
    repo: deb http://apt.kubernetes.io/ kubernetes-xenial main
    state: present

- name: Install Kubernetes packages
  ansible.builtin.apt:
    name: "{{ packages }}"
    state: present
    update_cache: true
  vars:
    packages:
      - kubelet
      - kubeadm
      - kubectl

- name: Mark Kubernetes packages as "hold"
  ansible.builtin.dpkg_selections:
    name: "{{ item }}"
    selection: hold
  loop:
    - kubelet
    - kubeadm
    - kubectl
