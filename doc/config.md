# Configuration Paramters

| Parameter                        | Type       | Description                                                                                                                             |
| -------------------------------- | ---------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| `cloud.provider`                 | String     | Cloud provider where resources will be created (Currently only `aws` is supported)                                                      |
| `cloud.aws.access_key`           | String     | AWS access key ID                                                                                                                       |
| `cloud.aws.secret_key`           | String     | AWS secret access key                                                                                                                   |
| `cloud.aws.region`               | String     | AWS region                                                                                                                              |
| `cloud.aws.vpc_id`               | String     | AWS VPC ID                                                                                                                              |
| `cloud.aws.vpc_subnet_ids`       | List       | AWS subnet IDs for specified VPC in `cloud.aws.vpc_id`                                                                                  |
| `cloud.aws.instance_type.master` | String     | AWS instance type for master nodes (Example: `t2.medium`)                                                                               |
| `cloud.aws.instance_type.worker` | String     | AWS instance type for worker nodes (Example: `t2.xlarge`)                                                                               |
| `cloud.aws.volume_size.worker`   | Integer    | Volume size in GB for worker nodes                                                                                                      |
| `cloud.aws.volume_size.master`   | Integer    | Volume size in GB for master nodes                                                                                                      |
| `cloud.aws.key_name`             | String     | AWS EC2 key pair name for SSH access                                                                                                    |
| `cloud.aws.certificate_arn`      | String     | AWS certificate ARN to be used in application load balancer                                                                             |
| `cloud.aws.tags`                 | Dictionary | Tags for AWS resources                                                                                                                  |
| `dns.create_records`             | Boolean    | If `true`, DNS records will be created automatically                                                                                    |
| `dns.provider`                   | String     | DNS provider (Currently only `cloudflare` is supported)                                                                                 |
| `dns.domain`                     | String     | Domain for DNS records                                                                                                                  |
| `dns.subdomains.app`             | String     | Subdomain for app                                                                                                                       |
| `dns.subdomains.jenkins`         | String     | Subdomain for Jenkins                                                                                                                   |
| `dns.subdomains.nexus`           | String     | Subdomain for Nexus Repository Manager                                                                                                  |
| `dns.subdomains.registry`        | String     | Subdomain for Docker container registry                                                                                                 |
| `dns.subdomains.k8s`             | String     | Subdomain for K8s API                                                                                                                   |
| `dns.cloudflare.api_token`       | String     | Cloudflare API token                                                                                                                    |
| `k8s.master_nodes`               | List       | Hostnames for K8s master nodes                                                                                                          |
| `k8s.worker_nodes`               | List       | Hostnames for K8s worker nodes                                                                                                          |
| `jenkins.admin_password`         | String     | Admin password for Jenkins                                                                                                              |
| `jenkins.job_name`               | String     | Name for the job to be used for build and deploy tasks on Jenkins                                                                       |
| `jenkins.repo_url`               | String     | Public Git repository URL of the app that the job in Jenkins will build and deploy                                                      |
| `nexus.admin_password`           | String     | Admin password for Nexus Repository Manager                                                                                             |
| `scm.provider`                   | String     | Source control management provider where a webhook will be created to trigger the job in Jenkins (Currently only `gitlab` is supported) |
| `scm.gitlab.access_token`        | String     | GitLab access token                                                                                                                     |
| `scm.gitlab.project_id`          | Integer    | GitLab project ID                                                                                                                       |
