# K8s DevOps Demo

This is a demo project to show how to create a DevOps environment using Jenkins and Nexus Repository Manager on Kubernetes.

The following operations are performed automatically without a single GUI interaction:

- Create resources required for Kubernetes installation
  - Create desired number of master/worker K8s instances on AWS EC2
  - Create AWS ELB load balancers to access ingress resources and K8s API
  - Create DNS records on Cloudflare for ingress resources and K8s API

- Install Kubernetes
  - Install Kubernetes dependecies
  - Initialize Kubernetes cluster with kubeadm on the first master node
  - Join other master nodes and worker nodes to cluster
  - Install Calico CNI plugin
  - Install Nginx Ingress Controller
  - Install OpenEBS for persistent storage

- Create a DevOps stack on Kubernetes cluster
  - Install Jenkins
  - Install Nexus Repository Manager with a Docker registry

- Create a CI/CD pipeline
  - Create a Jenkins build and deploy job for a public Git repository including a Jenkinsfile pipeline
  - Create a webhook on GitLab to trigger build and deploy job
  
## Quickstart

- Clone project to your environment:
  
  ```shell
   git clone https://gitlab.com/erdemolcay/k8s-devops-demo.git
   ```

- Copy config/config_sample.yml to config/config.yml and fill the parameters

- cd into ansible directory:

  ```shell
   cd k8s-devops-demo/ansible
   ```
- Run Ansible playbooks
  ```shell
   ansible-playbook playbooks/create-resources.yml
   ```
  ```shell
   ansible-playbook playbooks/create-k8s-cluster.yml
   ```
  ```shell
   ansible-playbook playbooks/deploy-devops-stack.yml
   ```
## Documentation

- ### [Configuration](./doc/config.md)
- ### [CI/CD Diagram](./doc/ci-cd-diagram.md)

## Demo

- ### Running environment:
  - Jenkins: ~~https://jenkins.olcay.net~~ (Not running anymore.)
  - Nexus Repository Manager: ~~https://nexus.olcay.net~~ (Not running anymore.)
  - Docker registry: ~~https://registry.olcay.net~~ (Not running anymore.)
  - App: ~~https://app.olcay.net~~ (Not running anymore.)
    - App is continiously built and deployed from https://gitlab.com/erdemolcay/simple-java-maven-app project

- ### Create resources:
  [![asciicast](https://asciinema.org/a/CFN3punlIblGeancaQmWcSuTu.svg)](https://asciinema.org/a/CFN3punlIblGeancaQmWcSuTu)

- ### Create K8s cluster:
  [![asciicast](https://asciinema.org/a/A026VL8t9WkmiSLwLLMnN8JuV.svg)](https://asciinema.org/a/A026VL8t9WkmiSLwLLMnN8JuV)

- ### Deploy DevOps Stack:
  [![asciicast](https://asciinema.org/a/1VcociFy1DdMNR3xWhuVoESqK.svg)](https://asciinema.org/a/1VcociFy1DdMNR3xWhuVoESqK)

## TODO:

- Add new cloud provider options
- Add new DNS service options
- Add new SCM service options
- Terraform implementation
- Specify Ansible requirements
- ~~Fix "No valid crumb was included in the request" error (apperantly an AWS Application Load Balancer issue) when Jenkins job is triggered remotely via webhook.~~ (Fixed. See: 45f75aa735aa2493a2dc1552c3c704758265b5ae)
